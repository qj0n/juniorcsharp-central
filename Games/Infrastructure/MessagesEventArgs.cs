﻿namespace Infrastructure
{
    public class MessagesEventArgs
    {
        public MessagesEventArgs(string result)
        {
            this.Result = result;
        }
        public string Result { get; private set;}
    }
}
