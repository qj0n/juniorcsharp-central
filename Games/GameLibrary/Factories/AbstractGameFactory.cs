﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure;

namespace GameLibrary
{
    public abstract class AbstractGameFactory
    {
        public abstract IGame CreateGameInstance();
        public abstract string Name { get; }
    }
    
    }

