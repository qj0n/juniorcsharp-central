﻿using System;
using Ninject.Modules;
using Infrastructure;
using GameLibrary;
using HighScore;
using Ninject.Extensions.Factory;
using Ninject.Extensions.Factory.Factory;

namespace DodatkowaPraca
{
    class ProgramModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IGamesProvider>().To<GamesProvider>().InSingletonScope();
            Bind<IScoreStrategy>().ToProvider(new ScoreManagerProvider());
            Bind<IHighScoreManager>().To<HighScoreManager>();

            Bind<AbstractGameFactory>().To<GuessGameFactory>();
            Bind<AbstractGameFactory>().To<MastermindGameFactory>();
            Bind<AbstractGameFactory>().To<ReflexGameFactory>();
            Bind<AbstractGameFactory>().To<TimeBasedGuessGameFactory>();
            Bind<AbstractGameFactory>().To<TimeBasedMastermindGameFactory>();

            //Bind<IGame>().To<GuessGame>();
            //Bind<IGame>().To<MastermindGame>();
            //Bind<IGame>().To<ReflexGame>();
            //Bind<IGame>().To<TimeBasedGuessGame>();
            //Bind<IGame>().To<TimeBasedMastermindGame>();
        }
    }
}
