﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Common;
using Microsoft.Win32;

namespace TextParser.UI
{
    public class MainViewModel : NotifyPropertyChanged
    {
        private string _sourcePath;
        private string _destinationPath;
        private IParser _currentParser;
        private CancellationTokenSource _cancellationTokenSource;
        private bool _canBeStarted = true;

        public MainViewModel()
        {
            SelectSourceCommand = new DelegateCommand(SelectSource);
            SelectDestinationCommand = new DelegateCommand(SelectDestination);
            StartCommand = new DelegateCommand(StartAsync, () => CanBeStarted);
            CancelCommand = new DelegateCommand(Cancel);

            LoadParsersAsync();
        }

        public bool CanBeStarted
        {
            get => _canBeStarted;
            set
            {
                if (value == _canBeStarted) return;
                _canBeStarted = value;
                OnPropertyChanged();
                StartCommand.CallCanExecuteChanged();
            }
        }

        private async void LoadParsersAsync()
        {
            var p = await Task.Run(() => new ParserManager().GetParsers());

            foreach (var parser in p)
                Parsers.Add(parser);
            CurrentParser = Parsers.FirstOrDefault();
        }

        private async void StartAsync()
        {
        
            using (_cancellationTokenSource = new CancellationTokenSource())
            {
                try
                {
                    CanBeStarted = false;
                    await CurrentParser.ParseFileAsync(SourcePath, DestinationPath,_cancellationTokenSource.Token);
                    MessageBox.Show("Zakończono");
                }
                catch (OperationCanceledException ex) when (ex.CancellationToken  == _cancellationTokenSource.Token)
                {
                    MessageBox.Show("Zatrzymano");
                }
                finally
                {
                    CanBeStarted = true;
                    _cancellationTokenSource = null;
                }
            }
        }

        private void Cancel()
        {
            _cancellationTokenSource?.Cancel();
        }

        private void SelectDestination()
        {
            var sfd = new SaveFileDialog();

            if (sfd.ShowDialog() == true)
            {
                DestinationPath = sfd.FileName;
            }
        }

        private void SelectSource()
        {
            var ofd = new OpenFileDialog();

            if (ofd.ShowDialog() == true)
            {
                SourcePath = ofd.FileName;
            }
        }

        public string SourcePath
        {
            get => _sourcePath;
            set
            {
                if (value == _sourcePath) return;
                _sourcePath = value;
                OnPropertyChanged();
            }
        }


        public string DestinationPath
        {
            get => _destinationPath;
            set
            {
                if (value == _destinationPath) return;
                _destinationPath = value;
                OnPropertyChanged();
            }
        }
        public ICommand SelectSourceCommand { get; }


        public ICommand SelectDestinationCommand { get; }

        public DelegateCommand StartCommand { get; }
        public ICommand CancelCommand { get; }


        public ObservableCollection<IParser> Parsers { get; } = new ObservableCollection<IParser>();

        public IParser CurrentParser
        {
            get => _currentParser;
            set
            {
                if (Equals(value, _currentParser)) return;
                _currentParser = value;
                OnPropertyChanged();
            }
        }

    }
}

