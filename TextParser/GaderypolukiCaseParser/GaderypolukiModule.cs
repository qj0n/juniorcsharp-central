﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;
using TextParser;

namespace GaderypolukiCaseParser
{
    public class GaderypolukiModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IParser>().To<GaderypolukiParser>();
        }
    }
}
