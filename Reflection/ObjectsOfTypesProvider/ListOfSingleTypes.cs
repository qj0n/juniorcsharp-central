﻿using System.Collections.Generic;
using Interfaces;
using JetBrains.Annotations;

namespace ObjectsOfTypesProvider
{
    public class ListOfSingleTypes : IGeneralType
    {
        public string GeneralName { get; }

        public List<ISingleTypeProvider> SingleType { get; }

        public ListOfSingleTypes([NotNull]string generalName, [NotNull, ItemNotNull]List<ISingleTypeProvider> singleType)
        {
            GeneralName = generalName;
            SingleType = singleType;
        }
    }
}