﻿using System.Reflection;
using Interfaces;
using JetBrains.Annotations;

namespace AssemblyProvider
{
    public class AssemblyPathOpener : IAssemblyOpener
    {
        public Assembly LoadedAssemblyTypes { get; }

        public AssemblyPathOpener([NotNull]IPathProvider pathToAssembly)
        {
            LoadedAssemblyTypes = Assembly.LoadFile(pathToAssembly.PathFile);
        }
    }
}
