﻿using System.Collections.Generic;
using System.Reflection;
using Interfaces;
using NUnit.Framework;
using ObjectsOfTypesProvider;

namespace NUnit.UnitTests.TypesInfoTests
{
    internal class TestClassSingleTypeBuilder : ISingleTypeBuilder
    {
        public string NameBuilder { get; set; }
        public List<FieldInfo> FieldsBuilderType { get; set; }
        public List<PropertyInfo> PropertiesBuilderType { get; set; }
        public List<MethodInfo> MethodsBuilderType { get; set; }
        public List<EventInfo> EventsBuilderType { get; set; }

        public TestClassSingleTypeBuilder(string name, List<FieldInfo> field, List<PropertyInfo> property, List<MethodInfo> method, List<EventInfo> even)
        {
            NameBuilder = name;
            FieldsBuilderType = field;
            PropertiesBuilderType = property;
            MethodsBuilderType = method;
            EventsBuilderType = even;
        }

        public TestClassSingleTypeBuilder()
        {
            NameBuilder = "";
            FieldsBuilderType = new List<FieldInfo>();
            PropertiesBuilderType = new List<PropertyInfo>();
            MethodsBuilderType = new List<MethodInfo>();
            EventsBuilderType = new List<EventInfo>();
        }

        public ISingleTypeProvider BuildNewSingleType()
        {
            return new SingleTypes(NameBuilder, FieldsBuilderType, PropertiesBuilderType, MethodsBuilderType, EventsBuilderType);
        }

        public void Clear()
        {
            NameBuilder = "";
            FieldsBuilderType = new List<FieldInfo>();
            PropertiesBuilderType = new List<PropertyInfo>();
            MethodsBuilderType = new List<MethodInfo>();
            EventsBuilderType = new List<EventInfo>();
        }
    }

    [TestFixture]
    public class SingleTypeBuilderTests
    {
        [Test]
        public void SingleTypeBuilder_BuildNewSingleTypeIstanceIsRight()
        {
            var singleType = new TestClassSingleTypeBuilder();

            var testSingleType = singleType.BuildNewSingleType();

            Assert.IsInstanceOf<ISingleTypeProvider>(testSingleType);
        }

        [Test]
        public void Property_IsReplacing()
        {
            var singleType = new TestClassSingleTypeBuilder("testName", new List<FieldInfo>(), new List<PropertyInfo>(), new List<MethodInfo>(), new List<EventInfo>());
            
            Assert.That(singleType.NameBuilder, Is.EqualTo("testName"));
        }

        [Test]
        public void ClearMethod_IsWorking()
        {
            var singleType = new TestClassSingleTypeBuilder("testName", new List<FieldInfo>(), new List<PropertyInfo>(), new List<MethodInfo>(), new List<EventInfo>());
            
            singleType.Clear();

            Assert.That(singleType.NameBuilder, Is.EqualTo(""));
        }
    }
}
