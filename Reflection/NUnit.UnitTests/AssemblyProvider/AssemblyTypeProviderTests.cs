﻿using System;
using System.IO;
using System.Reflection;
using AssemblyProvider;
using Interfaces;
using NUnit.Framework;

namespace NUnit.UnitTests.AssemblyProvider
{
    internal class TestPath : IPathProvider
    {
        public string PathFile { get; } = Path.Combine(new Uri(Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase) ?? string.Empty).LocalPath,@"AttributeProvider.dll");
    }

    public class AssemblyTypeProviderTests
    {
        [TestFixture]
        public class AssemblyTypeInfoTests
        {
            [Test]
            public void AssemblyName_IsMatch()
            {
                var obj = new AssemblyTypeProvider(new AssemblyPathOpener(new TestPath()));

                var stringName = obj.GeneralAssembly.GeneralName;

                Assert.That(stringName, Is.EqualTo(Assembly.LoadFrom(new TestPath().PathFile).FullName));
            }
        }
    }
}